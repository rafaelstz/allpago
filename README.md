 AllPago
----------------------------------------------
----------
Módulo criado para gerenciar as transações de pagamento utilizando comunicação entre o Gateway Allpago e lojas Magento, além de oferecer o serviço anti-fraude Fcontrol.

**Segue algumas informações importantes sobre o funcionamento do módulo de pagamento utilizando o Gateway allpago:**

O nosso Módulo de pagamento depende **DIRETAMENTE** do módulo `Mage_Payment`, que é uma estrutura base de um módulo de pagamento na estrutura do Magento. Isso significa que para garantir um perfeito funcionamento do nosso sistema é muito importante que não exista nenhuma customização direta neste módulo (alterações no Core da plataforma). Caso alguma classe seja extendida e customizada, deve ser analisada, pois, dependendo da customização, pode influenciar no funcionamento do módulo.

Existe também uma dependência direta da biblioteca **CURL** do php instalada no servidor para que o Magento faça comunicação com o Gateway Allpago.

O nosso Módulo permite a opção de autorização/captura automáticas, porém, essa funcionalidade depende diretamente que a **Cron** do servidor de hospedagem esteja rodando e o arquivo `Cron.php` da pasta raiz do Magento esteja adicionado na **crontab** do servidor. A Cron é um serviço que agenda tarefas em determinados períodos de tempo. No caso do nosso módulo é feito agendamento das autorizações e capturas.

Caso exista algum outro módulo de pagamento instalado em sua loja Magento, recomendamos a sua desativação, pois como todo módulo de pagamento depende diretamente do `Mage_Payment`, uma má construção deste módulo pode causar problemas no funcionamento do nosso módulo.

Caso exista algum módulo de checkout customizado instalado em sua loja e o nosso módulo não esteja funcionando corretamente, recomendamos que seja feito um teste desativando o módulo de checkout customizado, pois o mesmo pode ser a origem do problema de mal funcionamento do nosso módulo.

Junto ao nosso módulo é instalado o módulo Fcontrol. O funcionamento deste módulo depende diretamente do **SOAP**, sendo assim é necessário que esta opção esteja habilitada nas configurações do PHP.

No caso de problemas na instalação do módulo relacionados ao não aparecimento dos métodos de pagamento, parcelamento ou impressão de boleto no `success`, recomendamos a revisão da hierarquia do seu template. Recomendamos a instalação do módulo dentro do tema que está sendo utilizado. Por default ele é instalado em `“app/design/frontend/base/default/...”` para garantir que os nossos arquivos serão os carregados, carregue os em: `“app/design/frontend/meupacote/meutema/...”`. Mesma recomendação vale para pasta skin, que contém os estilos e imagens.

**Segue alguns requisitos de ambiente (em casos de cloud) para garantir o perfeito funcionamento do módulo e da sua loja Magento:**

Instalação do php-curl e php-cli, php5-mycript, php5-gd;
- Alterar `memory_limit` do **php** para `256M`;
- Ativar **mod_rewrite** do apache (`a2enmod rewrite`)
- Permissões Magento:

    find . -type f -exec chmod 644 {}\;
    find . -type d -exec chmod 755 {} \ ;
    chmod o+w var var/.htaccess app/etc
    chmod -R o+w media


----------
© 2014 [AllPago](http://www.allpago.com.br/). Todos os direitos reservados.